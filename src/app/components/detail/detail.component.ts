import { Component, OnInit } from '@angular/core';
import { MoviesService } from 'src/app/services/movies.service';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  pelicula: any;
  trustedUrl: any;
  comebackTo: string = "";
  buscado: string = "";

  constructor(private moviesService: MoviesService, private router: ActivatedRoute, private sanitizer: DomSanitizer) {

    this.router.params.subscribe(params => {
      console.log(params)
      this.comebackTo = params.page;
      this.buscado = params.buscado;
    })

    this.router.params.subscribe(param => {
      this.moviesService.getMovieById(param.id).subscribe(res => {
        this.pelicula = res;
      });
    })
  }

  ngOnInit() {
  }

  getImage() {
    return "http://image.tmdb.org/t/p/w500" + this.pelicula.poster_path;
  }

}
