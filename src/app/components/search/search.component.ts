import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MoviesService } from 'src/app/services/movies.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  buscar: string = "";
  buscadas: any;
  searching = false;

  constructor(private moviesService: MoviesService, private router: ActivatedRoute) {

    this.router.params.subscribe(param => {
      if (param.texto !== '') {
        this.buscar = param.texto;
        this.buscarPelicula()
      }
    })
  }

  ngOnInit() {
  }

  buscarPelicula() {
    if (this.buscar == undefined) {
      return
    }
    if (this.buscar.length == 0) {
      return
    }

    // Limpio screen y muestro el loading.
    this.buscadas = null; 
    this.searching = true;

    this.moviesService.searchMovie(this.buscar).subscribe(res => {
      this.buscadas = res;
      this.searching = false;
    });

  }

}
