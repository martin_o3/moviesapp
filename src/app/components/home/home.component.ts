import { Component, OnInit } from '@angular/core';
import { MoviesService } from 'src/app/services/movies.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  movies: any;
  onCinemas: any;

  constructor(private moviesServices: MoviesService) {
  }

  ngOnInit() {
    this.moviesServices.getPopulars().subscribe(res => {
      this.movies = res;
      console.log(this.movies);
      //this.movies.results.splice(10, 10);
    })

    this.moviesServices.getOnCinemas().subscribe(res => {
      this.onCinemas = res;
      //this.onCinemas.results.splice(10, 10);
    })
  }

}
