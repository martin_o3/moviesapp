import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  private apiKey = "427d6ea5b7dc2ca899bed30ef1608309";
  private urlApi = "https://api.themoviedb.org/3/"

  constructor(private http: HttpClient) { }

  getAllMovies() {
    let url = `${this.urlApi}movie/550?api_key=427d6ea5b7dc2ca899bed30ef1608309`;
    return this.http.get(url);
  }

  getPopulars() {
    let url = `${this.urlApi}discover/movie?sort_by=popularity.desc&api_key=${this.apiKey}&language=es`;
    return this.http.get(url);
  }

  getMovieById(id: string) {
    let url = `${this.urlApi}movie/${id}?&api_key=${this.apiKey}&language=es`;
    return this.http.get(url);
  }

  getOnCinemas() {
    let desde = new Date();
    let hasta = new Date();

    hasta.setDate(hasta.getDate() + 8);

    // Completando el formato requerido con el 0.
    let lMonthDesde = ('0' + (desde.getMonth() + 1)).slice(-2)
    let lMonthHasta = ('0' + (hasta.getMonth() + 1)).slice(-2)

    let lDayDesde = ('0' + (desde.getDate())).slice(-2)
    let lDayHasta = ('0' + (hasta.getDate())).slice(-2)

    // Completando fecha en formato String.
    let desdeStr = `${desde.getFullYear()}-${lMonthDesde}-${lDayDesde}`
    let hastaStr = `${hasta.getFullYear()}-${lMonthHasta}-${lDayHasta}`

    // OPCIÓN 2, podría haber usado el formato ISO: 
    // let desdeStr = desde.toISOString().substring(0,10);
    // let hastaStr = hasta.toISOString().substring(0,10);

    let url = `${this.urlApi}discover/movie?primary_release_date.gte=${desdeStr}&primary_release_date.lte=${hastaStr}&api_key=${this.apiKey}&language=es`;
    return this.http.get(url);
  }


  searchMovie(texto: string) {
    let url = `${this.urlApi}search/movie?query=${texto}&sort_by=popularity.desc&api_key=${this.apiKey}&language=es`;
    return this.http.get(url);
  }

}